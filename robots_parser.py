import requests
import re


class RobotsParser:
    def __init__(self, robots_url, user_agent='*'):
        self.robots_url = robots_url
        self.permissions = []
        self.robots_text = ''

        try:
            url_response = requests.get(robots_url)
            if url_response.status_code >= 400:
                return
        except Exception:
            return

        self.robots_text = url_response.text
        self.parse_robots(user_agent=user_agent)

    def parse_robots(self, user_agent='*'):
        if self.robots_text.lower().find('user-agent: ' + user_agent) < 0:
            return []

        self.robots_text = self.robots_text[self.robots_text.lower().find('user-agent: ' + user_agent) + 13 + len(user_agent):]
        if self.robots_text.find('User-agent:') >= 0:
            self.robots_text = self.robots_text[0:self.robots_text.lower().find('user-agent:')]

        robots_text_split = self.robots_text.split('\n')
        for value in robots_text_split:
            if value.startswith('Allow: ') is True:
                self.permissions.append({'path': value[7:], 'can_crawl': True})
            if value.startswith('Disallow: ') is True:
                self.permissions.append({'path': value[10:], 'can_crawl': False})

    def can_crawl_path(self, path):
        try:
            if path == '/robots.txt':
                return True

            for permission in self.permissions:
                # For direct matches
                if path == permission['path']:
                    return permission['can_crawl']

                # For wildcard matches  (* and $)
                if permission['path'].find('*') >= 0 or permission['path'].find('$') >= 0:
                    if len(re.findall(permission['path'].replace('*', '.*'), path)) > 0:
                        return permission['can_crawl']

                # For folder matches
                if permission['path'].endswith('/'):
                    if path.startswith(permission['path']) is True:
                        return permission['can_crawl']

            return True

        except Exception as e:
            return True


if __name__ == '__main__':
    # Example usage:
    roboto = RobotsParser(robots_url='https://en.wikipedia.org/robots.txt', user_agent='*')
    print(roboto.permissions)
    print(roboto.can_crawl_path('/w/'))
