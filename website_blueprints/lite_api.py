from flask import Blueprint, render_template
import html
from website_blueprints.query_decorator import *
import search_engines.infinity.infinity as Infinity
from website_blueprints.results_api import get_external_node_results, remove_duplicates_from_list
lite_api = Blueprint('lite_api', __name__)

# Results that do not use any JS
@lite_api.route('/lite/results', methods=['GET', 'POST'])
def render_lite_results():
    try:
        query_data = get_query_data(request)
        query = query_data['query']
        query = html.unescape(query)
        page_number = query_data['page_number']


        external_node_endpoints = []
        external_node_results = []
        if 'external_nodes' in request.form:
            external_node_endpoints = html.unescape(request.form.get('external_nodes')).split()
            external_node_results = get_external_node_results(external_node_endpoints, query)

        results = Infinity.get_infinity_results_es(query, 10, 10, page_number)
        results += external_node_results
        results = remove_duplicates_from_list(results)

        return render_template('results/lite/results.html', query=query, results=results,
                               page_number=page_number, current='infinity', external_node_endpoints=external_node_endpoints)
    except Exception:
        return render_template('results/lite/results.html', query='', results=[],
                               page_number=1, current='infinity')

