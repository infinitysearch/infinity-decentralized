from flask import Blueprint, render_template, redirect, send_file, jsonify
import requests
import io
import html
from urllib.parse import urlparse
from func_timeout import func_timeout, FunctionTimedOut
from website_blueprints.query_decorator import *
import helpers.query_analyzer as QueryAnalyzer
import search_engines.infinity.infinity as Infinity

results_api = Blueprint('results_api', __name__)

# If you want to monetize your instance with the web monetization standard (https://webmonetization.org), set this
# variable to your payment pointer. More about how this works can be found at https://coil.com/creator/how-to-monetize.
# Leaving this variable blank will not change any behavior of Infinity Decentralized.
web_monetization_payment_pointer = ''

def remove_duplicates_from_list(results):
    clean_results = []
    for result in results:
        passed = True
        for clean_result in clean_results:
            if result['url'] == clean_result['url']:
                passed = False
                break
            if result['url'][:-1] == clean_result['url'] or result['url'] == clean_result['url'][:-1]:
                passed = False
                break
            if result['url'].startswith('www.') is True and clean_result['url'].startswith('www.') is False:
                if result['url'][4:] == clean_result['url'] or result['url'] == clean_result['url']:
                    passed = False
                    break
            if result['url'].startswith('www.') is False and clean_result['url'].startswith('www.') is True:
                if result['url'] == clean_result['url'] or result['url'] == clean_result['url'][4:]:
                    passed = False
                    break

        if passed is True:
            clean_results.append(result)

    return clean_results


def display_homepage():
    return render_template('pages/home.html')


@results_api.route('/', methods=['GET', 'POST'])
def render_static():
    if request.method == 'POST':
        return render_results()
    else:
        try:
            if request.args.get('q') is not None:
                query = request.args.get('q')
                return redirect('/results?q=' + query)

        except Exception:
            return display_homepage()

        return display_homepage()


def remove_blacklisted_urls_from_list(results, blacklist):
    new_results = []
    removed_links = 0
    for result in results:
        passed = True
        for blacklisted_url in blacklist:
            if blacklisted_url == '' or blacklisted_url is None:
                continue
            if result['url'].find(blacklisted_url) >= 0:
                passed = False
                removed_links += 1
                break

        if passed is False:
            continue
        new_results.append(result)

    return new_results, removed_links


def get_external_node_results(urls, query, page_number=1):
    results = []
    for url in urls:
        try:
            parsed = urlparse(url)
            results += requests.get(url + '?q=' + query + '&page_number=' + str(page_number), timeout=3).json()
        except Exception:
            continue

    results = remove_duplicates_from_list(results)
    return results


@results_api.route('/web_results', methods=['GET', 'POST'])
def render_results():
    query_data = get_query_data(request)
    query = query_data['query']
    page_number = query_data['page_number']
    limit_results = True

    if len(query) == 0:
        return render_template('results/fetch.html', query=query,
                               instant_extracts=[], bing_results=[], definition=[],
                               current='web',
                               calculator=[], page_number=page_number,
                               limit_results=limit_results, infinity_info_results=[], external_node_endpoints=[],
                               html_editor=[], base_converter=[], unit_converter=[], removed_links=0)

    advanced_search_query = False

    query = html.unescape(query)
    if 'advanced_search_query' in dict(request.form):
        advanced_search_query = True

    external_node_endpoints = []
    external_node_results = []

    if 'external_nodes' in request.args:
        external_node_endpoints = request.args.get('external_nodes').split('\n')
    if 'external_nodes' in request.form:
        external_node_endpoints = request.form.get('external_nodes').split('\n')

    if len(external_node_endpoints) > 0:
        external_node_results = get_external_node_results(external_node_endpoints, query, page_number)

    results = Infinity.get_infinity_results_es(query, 20, 10, page_number)
    if len(results) == 0:
        results = Infinity.get_infinity_results_es('"'+ query + '"', 20, 10, page_number)
    results += external_node_results
    results = remove_duplicates_from_list(results)

    calculation = []
    html_editor = []
    definition = []
    base_converter = []
    unit_converter = []
    color_picker = []
    rand_num_generator = []
    stopwatch_timer = []

    try:
        integrations = QueryAnalyzer.analyze_query_v2(query)
    except Exception:
        integrations = '', ''

    if integrations[0] == 'calculator':
        calculation = [[]]
    elif integrations[0] == 'html_editor':
        html_editor = [[]]
    elif integrations[0] == 'base_converter':
        base_converter = [[]]
    elif integrations[0] == 'unit_converter':
        unit_converter = [[]]
    elif integrations[0] == 'color_picker':
        color_picker = [[]]
    elif integrations[0] == 'rand_num_generator':
        rand_num_generator = [[]]
    elif integrations[0] == 'stopwatch_timer':
        stopwatch_timer = [[]]

    removed_links = 0
    if 'blacklist' in request.cookies and request.cookies.get('blacklist') != '':
        blacklist = request.cookies.get('blacklist').replace('%20', '')
        blacklist = blacklist.split('%0A')
        results = remove_blacklisted_urls_from_list(results, blacklist)
        removed_links += results[1]
        results = results[0]

    return render_template('results/fetch.html', query=query,
                           results=results, definition=definition,
                           current='web', removed_links=removed_links,
                           calculator=calculation, page_number=page_number, color_picker=color_picker,
                           limit_results=limit_results, html_editor=html_editor,
                           base_converter=base_converter, unit_converter=unit_converter,
                           rand_num_generator=rand_num_generator, stopwatch_timer=stopwatch_timer,
                           advanced_search_query=advanced_search_query, external_node_endpoints=external_node_endpoints)


@results_api.route('/results', methods=['GET', 'POST'])
def render_results_layout():
    query_data = get_query_data(request)
    query = query_data['query']
    page_number = query_data['page_number']

    advanced_search_query = False
    if 'advanced' in query_data['advanced_params']:
        advanced_search_query = True

    external_node_endpoints = []
    if 'external_nodes' in request.form:
        external_node_endpoints = html.unescape(request.form.get('external_nodes')).split()

    return render_template('results/layout.html', query=query, url='/web_results',
                           current='web', page_number=page_number, advanced_search_query=advanced_search_query,
                           advanced_params=query_data['advanced_params'],
                           external_node_endpoints=external_node_endpoints,
                           web_monetization_payment_pointer=web_monetization_payment_pointer)


@results_api.route('/results_json')
def render_search_results_json():
    query_data = get_query_data(request)
    query = query_data['query']
    page_number = query_data['page_number']
    results = []

    if len(query) == 0:
        return jsonify(results)

    results = Infinity.get_infinity_results_es(query, 10, 10, page_number)
    if len(results) == 0:
        results = Infinity.get_infinity_results_es('"'+ query + '"', 20, 10, page_number)
    return jsonify(results)


def load_image(url):
    image = io.BytesIO(requests.get(url).content)
    return image


@results_api.route('/proxy/image')
def render_external_image():
    if request.args.get('url') is None:
        return ''
    try:
        image = None
        try:
            image = func_timeout(5, load_image, args=(request.args.get('url'),))
        except FunctionTimedOut:
            pass
        except Exception as e:
            pass

        if image is None:
            return send_file('/images/misc/image_error_64.png', attachment_filename='external_image.png',
                             mimetype='image/png')
        return send_file(image, attachment_filename='external_image.png', mimetype='image/png')
    except Exception:
        return ''


