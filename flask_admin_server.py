from flask import Flask, render_template, redirect, request, make_response
import uuid
from functools import wraps
import admin_operations
import time
import html

app = Flask(__name__)

admin_api_key = 'insert_your_api_key_and_you_must_rename_this'
admin_api_key = 'askldjfasl'

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'infinity_admin_cookie' not in request.cookies:
            return redirect('/auth')

        session_cookie = open('session_cookie.txt', 'r').read()

        if 'infinity_admin_cookie' in request.cookies and request.cookies.get('infinity_admin_cookie') != session_cookie:
            return redirect('/auth')

        return f(*args, **kwargs)
    return decorated_function


@app.route('/toggle_crawler')
@login_required
def toggle_crawler():
    crawler_status = admin_operations.get_crawler_status()
    if crawler_status == 'running':
        admin_operations.stop_crawler()
    else:
        admin_operations.start_crawler()

    time.sleep(3)
    return redirect('/')


@app.route('/add_url')
@login_required
def add_url():
    if 'url' in request.args:
        admin_operations.add_url(request.args.get('url'))

    time.sleep(1)
    return redirect('/')


@app.route('/remove_url')
@login_required
def remove_url():
    if 'url' in request.args:
        admin_operations.remove_url(request.args.get('url'))

    time.sleep(1)
    return redirect('/')


@app.route('/reset_index')
@login_required
def reset_index():
    admin_operations.reset_index()
    return redirect('/')


@app.route('/confirm_reset_index')
@login_required
def confirm_reset_index():
    return render_template('crawler_admin/confirm_reset.html')


@app.route('/update_sites_to_crawl')
@login_required
def update_sites_to_crawl():
    if 'sites_to_crawl' in request.args:
        sites_to_crawl = html.unescape(request.args.get('sites_to_crawl')).split()
        sites_to_crawl = list(set(sites_to_crawl))
        admin_operations.update_sites_to_crawl(sites_to_crawl)
        time.sleep(2)

    return redirect('/')


@app.route('/', methods=['GET', 'POST'])
@login_required
def render_search_results():
    size_in_gb, document_count = admin_operations.get_db_size_info()
    most_recent_additions = admin_operations.get_most_recently_added_webpages()
    websites_to_crawl = str(open('sites_to_crawl.txt', 'r').read()).split('\n')
    crawler_status = admin_operations.get_crawler_status()

    return render_template('crawler_admin/dashboard.html', size_in_gb=size_in_gb, document_count=document_count,
                           websites_to_crawl=websites_to_crawl, crawler_status=crawler_status,
                           most_recent_additions=most_recent_additions)


@app.route('/auth', methods=['GET', 'POST'])
def render_auth():
    if 'api_key' in request.args:
        if request.args.get('api_key') == admin_api_key:
            user_cookie = str(uuid.uuid4())
            open('session_cookie.txt', 'w').write(user_cookie)
            resp = make_response(redirect('/'))
            resp.set_cookie('infinity_admin_cookie', user_cookie, samesite='strict')
            return resp

    return render_template('crawler_admin/auth.html')


if __name__ == '__main__':
    app.run(port=5001)
