from urllib.parse import urlparse
from elasticsearch import Elasticsearch
from func_timeout import func_timeout, FunctionTimedOut

def detected_advanced_query(query):
    if query.find('"') >= 0 or query.find("'") >= 0 or query.find('(') >= 0 or query.find('OR') >= 0 \
            or query.find('AND') >= 0 or query.find('-') >= 0 or query.find('+') >= 0 or query.find('filetype:') >= 0\
            or query.find('site:') >= 0:
        return True

    return False


def search_infinity_results_es(query, limit, page_number):
    es = Elasticsearch()

    if detected_advanced_query(query) is False:
        es_query = {'query': {'multi_match': {'query': query, 'fields': ['title', 'desc', 'url']}}, 'size': limit,
                    'from': (page_number - 1) * limit}
    else:
        es_query = {"query": {"query_string": {"query": query}}, 'size': limit, 'from': (page_number - 1) * limit}
    response = es.search('infinity_decentralized', body=es_query)

    formatted_results = []
    try:
        results = response['hits']['hits']
        for res in results:
            parsed = urlparse('https://' + res['_source']['url'])
            tld = parsed[1].split('.')
            tld = tld[-2] + '.' + tld[-1]
            favicon = '/proxy/image?url=' + 'https://' + tld + '/favicon.ico'
            formatted_results.append(
                {'url': res['_source']['url'], 'title': res['_source']['title'], 'desc': res['_source']['description'],
                 'favicon': favicon})
    except Exception:
        pass

    return formatted_results



def get_infinity_results_es(query, timeout_time, limit=15, page_number=1):
    try:
        results = func_timeout(timeout_time, search_infinity_results_es, args=(query, limit, page_number))
    except FunctionTimedOut:
        results = []
    except Exception as e:
        results = []

    return results

