from elasticsearch import Elasticsearch
import pymongo
import subprocess
import os
import url_analysis


def get_crawler_status():
    crawler_pid = str(open('crawler_pid.txt', 'r').read())
    pid_status_from_shell = subprocess.check_output('ps aux | grep ' + str(crawler_pid), shell=True).decode()
    if pid_status_from_shell.find('crawler') >= 0:
        return 'running'
    return 'stopped'


def stop_crawler():
    crawler_pid = str(open('crawler_pid.txt', 'r').read())
    os.system('kill ' + str(crawler_pid))

def start_crawler():
    stop_crawler()
    os.system('nohup python3.7 -u -m crawler > /dev/null &')


def reset_index():
    stop_crawler()

    client = pymongo.MongoClient()
    db = client['infinity_decentralized']
    col = db['infinity_decentralized']
    es = Elasticsearch()

    col.delete_many({})
    es.delete_by_query('infinity_decentralized', {'query': {'match_all': {}}})


def add_url(url):
    client = pymongo.MongoClient()
    db = client['infinity_decentralized']
    col = db['infinity_decentralized']
    es = Elasticsearch()

    shortened_link = url_analysis.shorten_link(url)
    url_analysis_result = url_analysis.get_page_information('http://' + shortened_link)

    if url_analysis_result[0] is False:
        col.update_one({'url': shortened_link}, {'$set': {'uploaded': False, 'good_link': False, 'passed': True}}, True)
        return

    url_info = dict(url_analysis_result[1])
    url_info['url'] = shortened_link

    es_response = es.index('infinity_decentralized', url_info)

    url_info['uploaded'] = False
    url_info['good_link'] = True
    url_info['passed'] = False

    try:
        if es_response['_shards']['successful'] > 0:
            url_info['uploaded'] = True
        else:
            'Submission FAILED!'
    except Exception as e:
        pass

    col.update_one({'url': shortened_link}, {'$set': url_info}, True)


def remove_url(url):
    client = pymongo.MongoClient()
    db = client['infinity_decentralized']
    col = db['infinity_decentralized']
    es = Elasticsearch()
    url = url_analysis.shorten_link(url)
    col.delete_many({'url': url})
    es.delete_by_query('infinity_decentralized', body={'query': {'bool': {'must': [{'term': {'url': url}}]}}})


def update_sites_to_crawl(sites):
    sites_to_crawl_string = ''
    for site in sites:
        sites_to_crawl_string += site + '\n'
    sites_to_crawl_string = sites_to_crawl_string[:-1]
    open('sites_to_crawl.txt', 'w').write(sites_to_crawl_string)


def get_db_size_info():
    client = pymongo.MongoClient()
    db = client['infinity_decentralized']

    index_data = db.command("collstats", 'infinity_decentralized')
    size_in_gb = index_data['size'] / (1024 * 1024 * 1024)
    document_count = index_data['count']

    return size_in_gb, document_count


def get_most_recently_added_webpages():
    client = pymongo.MongoClient()
    db = client['infinity_decentralized']
    col = db['infinity_decentralized']
    results = col.find({'uploaded': True}).sort('_id', pymongo.DESCENDING).limit(5)
    return list(results)




if __name__ == '__main__':
    # reset_index('infinity_decentralized')
    # get_db_size_info('infinity_decentralized')
    print(get_crawler_status())
